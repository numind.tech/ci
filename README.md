# Docker images for CI

List of images used by NuMind for its Continuous Integration & Deployment.

Dockerfiles are stored in the git repository and are build & store in the
container repository of this project.
