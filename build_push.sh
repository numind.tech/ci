#!/usr/bin/env -S bash -e

version=$(grep "ARG conveyor_version=" Dockerfile_package | cut -d '=' -f2)
./Dockerfile_package
docker tag registry.gitlab.com/numind.tech/ci/package registry.gitlab.com/numind.tech/ci/package:$version
docker push registry.gitlab.com/numind.tech/ci/package:latest
docker push registry.gitlab.com/numind.tech/ci/package:$version
